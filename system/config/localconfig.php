<?php

### INSTALL SCRIPT START ###
$GLOBALS['TL_CONFIG']['licenseAccepted'] = true;
$GLOBALS['TL_CONFIG']['installPassword'] = '$2y$10$ZT1PcZOheG58oWsvJIyBo.D14Af/xs1wXkaLiwsd9Lka.dL8352ei';
$GLOBALS['TL_CONFIG']['encryptionKey'] = '306fa69a18c667299c362eeee0251fa3';
$GLOBALS['TL_CONFIG']['dbDriver'] = 'MySQLi';
$GLOBALS['TL_CONFIG']['dbHost'] = 'localhost';
$GLOBALS['TL_CONFIG']['dbUser'] = 'root';
$GLOBALS['TL_CONFIG']['dbPass'] = 'root';
$GLOBALS['TL_CONFIG']['dbDatabase'] = 'contao';
$GLOBALS['TL_CONFIG']['dbPconnect'] = false;
$GLOBALS['TL_CONFIG']['dbCharset'] = 'UTF8';
$GLOBALS['TL_CONFIG']['dbPort'] = 3306;
$GLOBALS['TL_CONFIG']['dbSocket'] = '';
$GLOBALS['TL_CONFIG']['adminEmail'] = 'yannik@idbakery.de';
$GLOBALS['TL_CONFIG']['maintenanceMode'] = false;
### INSTALL SCRIPT STOP ###
